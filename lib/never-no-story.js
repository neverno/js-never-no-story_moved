'use strict';

var Promise = require('bluebird');
var xmlrpc = require('xmlrpc');
var uuid = require('uuid');
var lookupMimeType = require('./lookup-mime-type');
var request = require('request');

function NeverNoStory(hostname, xmlrpcPort, restPort)
{
    if (restPort == null) restPort = 9292;
    if (xmlrpcPort == null) xmlrpcPort = 9191;

    this.hostname = hostname;
    this.xmlrpcPort = xmlrpcPort;
    this.messagesApiRestPort = restPort;

    this.xmlrpcUrl = 'http://' + hostname + ':' + xmlrpcPort;

    this.restBaseUrl = 'http://' + hostname + ':' + restPort; 
    this.messagesRestUrl = this.restBaseUrl + '/messages';

    this.client = xmlrpc.createClient(this.xmlrpcUrl);
}

function handleXmlRpcResponse(resolve, reject, callback, err, res)
{
    if (err != null || (res != null && res.ok != 'ok'))
    {
        if (err == null)
            err = new Error('XML-RPC call failed'); // typically {'ok': 'fail'}

        reject(err);
        if (callback != null && typeof callback == 'function') callback(err, res);
    }
    else
    {
        resolve(res);
        if (callback != null && typeof callback == 'function') callback(null, res);
    }
}

function convertLocation(geo)
{
    if (geo == null || typeof geo != 'object') return undefined;
    var lat = parseFloat(geo.lat || geo.latitude);
    var lon = parseFloat(geo.lon || geo.lng);
    return (lat != 0 && lon != null) ? { lat: lat, lon: lon } : undefined;
}

function convertDate(date, div)
{
    if (div == null) div = 1;
    if (date == null) return undefined;

    if (date instanceof Date) return Math.round(date.getTime() / div);

    var d = Date.parse(date);
    if (isNaN(d)) d = parseInt(date);
    return isNaN(d) ? undefined : Math.round(new Date(d).getTime() / div);
}

function convertAttachment(att, generic)
{
    var mt;
    if (typeof att == 'string')
    {
        mt = lookupMimeType(att);
    }
    else
    {
        var m = att.type.match(/^(.*?)\/(.*?)$/);
        mt = {
            class: (m == null) ? 'application' : m[1],
            type: (m == null) ? 'octet-stream' : m[2], 
            mimeType: (m == null) ? 'application/octet-stream' : att.type
        }
        att = att.url;
    }   

    var a = (generic === true) ? {
        type: mt.class,
        content_type: mt.mimeType,
        url: att,
        display_url: att,
        expanded_url: att
    } : {
        type: mt.class,
        href: att,
        src: att,
    };

    if (mt.class == "image")
    {
        if (generic !== true) a.type = "photo";
        return a;
    }
    else if (mt.class == "video")
    {
        return a;
    }
    else
        return undefined;
}

NeverNoStory.prototype._submitMessageRaw = function(searchId, sourceType, payload, resolve, reject, callback)
{
    var me = this;

    var args =
    {
        message_id: uuid(),
        search_id: (searchId || -1).toString(),
        queue_id: "0", // legacy: Interactivity Desk
        source: (sourceType || "").toString(),
        message_payload: new Buffer(JSON.stringify(payload)).toString("base64")
    }

    // console.log("\n-- args is --\n"
    //     + JSON.stringify(args, null, '  ')
    //     + "\n-- payload pre-base64 is --\n"
    //     + JSON.stringify(payload, null, '  ')
    //     + "\n\n"
    // )

    me.client.methodCall('message.submit', [ args ], function(err, res)
    {
        handleXmlRpcResponse(resolve, reject, callback, err, res);
    })
}

NeverNoStory.prototype.submitMessageRaw = function(searchId, sourceType, payload, callback)
{
    var me = this;
    return new Promise(function(resolve, reject)
    {
        me._submitMessageRaw(searchId, sourceType, payload, resolve, reject, callback);
    })    
}

NeverNoStory.prototype.submitMessage = function(searchId, sourceType, msg, callback)
{
    var me = this;    

    if (sourceType == 'generic')
        return me.submitGenericMessage(searchId, msg, callback);

    return new Promise(function(resolve, reject)
    {
        var id = msg.id || uuid();

        var payload =
        {
            id: id || null,
            link: msg.url || null,
            from:
            {
                id: msg.author.id || uuid(),
                name: msg.author.name || null,
                username: msg.author.username || null,
                is_verified: false,
                link: msg.author.url || null,
                url: msg.author.url || null,
                pic: msg.author.picUrl || null,
                pic_big: msg.author.picUrl || null
            },
            message: (msg.text || "").toString(),
            type: (msg.type || "status").toString(),
            post_id: id || null,
            avatar_url: msg.author.picUrl || null,
            created_time: convertDate(msg.created, 1000) || null,
            updated_time: convertDate(msg.modified || msg.updated, 1000) || null,
        }

        if (msg.attachments != null && msg.attachments instanceof Array)
        {
            payload.attachments = { "media": [] };
            msg.attachments.forEach(function(att)
            {
                var m = convertAttachment(att, false);
                if (m != null)
                    payload.attachment.media.push(a);
            });

            if (payload.attachments.media.length == 0)
                delete payload.attachments; 
        }

        return me._submitMessageRaw(searchId, sourceType, payload, resolve, reject, callback)
    })
}

NeverNoStory.prototype.submitGenericMessage = function(searchId, msg, callback)
{
    var me = this;
    return new Promise(function(resolve, reject)
    {
        var doc =
        {
            "document_type": "GenericMessage",
            "search_id": searchId,
            "source": "Moderator",
            "source_type": "Rendered",
            "author": (msg.author.name || "").toString(),
            "nickname": (msg.author.username || "").toString(),
            "message": (msg.text || "").toString(),
            "timestamp": convertDate(msg.created || Date.now()),
            "created": convertDate(msg.created || Date.now()),
            "profile_image_url": msg.author.picUrl,
            "images": [],
            "videos": [],
            "current_format_id": 0,
            "format_data": null,
            "id": msg.id || uuid(),
            "origin": null,
            "links": null,
            "location": convertLocation(msg.geo || msg.location),
            "profile_id": msg.author.id,
            "permalink": msg.url,
            "hashtags": null,
            "usertags": null,
            "counts": null,
            "original": null
        };

        if (msg.attachments && msg.attachments.length > 0)
        {            
            msg.attachments.forEach(function (att, index)
            {
                var a = convertAttachment(att, true);
                if (a != null)
                {
                    if (a.type == 'image') doc.images.push(a);
                    else if (a.type == 'video') doc.videos.push(a);
                }
            });
        }

        request.post(
        {
            url: me.messagesRestUrl,
            headers:
            {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(doc)
        },
        function(err, resp, body)
        {
            if (err)
            {
                reject(err);
                if (callback != null && typeof callback == 'function') callback(err, body);
            }
            else if (resp.statusCode == 201 || resp.statusCode == 201)
            {
                var data = JSON.parse(body);

                resolve(data);
                if (callback != null && typeof callback == 'function') callback(null, data);
            }
            else
            {
                var e = new Error("Unexpected HTTP status " + resp.statusCode, + "while posting generic message to Story endpoint");

                reject(e)
                if (callback != null && typeof callback == 'function') callback(e, body);
            }
        });
    })
}

NeverNoStory.prototype.submitVote = function(pollId, searchId, votes, callback)
{
    var me = this;
    return new Promise(function(resolve, reject)
    {
        var args =
        {
            message: uuid(), // otherwise we get a ex:nil value back for 'id'
            poll_id: (pollId || -1).toString(), // string!
            search_id: (searchId || -1).toString(), // ..string!
            properties:
            {
            }
        };

        Object.keys(votes).forEach(function(uuid, index)
        {
            args.properties["OPTION_" + (1+index)] = (uuid || "ERROR").toString();
            args.properties["COUNT_" + (1+index)] = (votes[uuid] || 0).toString(); // ..string!!
        });

        me.client.methodCall('poll.submit', [ args ], function(err, res)
        {
            handleXmlRpcResponse(resolve, reject, callback, err, res);
        })
    })
}

module.exports = NeverNoStory;
