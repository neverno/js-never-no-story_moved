var mimeTypes = require('mime-types');

/**
 * Common implementation of MIME type lookup with a bit of post processing.
 *
 * If no match, will default to application/octet-stream and set success=false
 *
 * .class = whatever before /
 * .type = whatever after /
 * .mimeType = the/whole-thing
 *
 * @param {string} fn - Filename or url to process
 * @returns {{success: boolean, class: string, type: string, mimeType: string}}
 */
function lookupMimeType(fn)
{
    var res = { success: true };
    var mt = mimeTypes.lookup(fn);
    if (mt === false) { mt = "application/octet-stream"; res.success = false; }
    var ss = mt.split(/\//);
    res.class = ss[0];
    res.type = ss[1];
    res.mimeType = mt;
    //console.log("lookupMimEeType(%s) -> %s", fn, JSON.stringify(res))
    return res;
}

module.exports = lookupMimeType;
