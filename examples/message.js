var NeverNoStory = require('../lib/never-no-story');

var story = new NeverNoStory("storyrunetest.never.no");

story.submitGenericMessage(
  41, // search id
  {
    author: {
      username: 'billg512k',
      name: 'Bill Gates',
      picUrl: 'http://1.vgc.no/drfront/images/2016/09/13/c=596,53,667,667;w=316;h=316;298101.jpg' // optional
    },
    geo: { // optional
      lat: 59.9142366,
      lon: 10.7530902
    },
    text: '640k should be enough for everyone, right?',
    created: new Date(), // optional
    modified: new Date(), // optional
    attachments: [ // optional
      { type: 'image/jpeg', url: 'http://vg.no/annonsorinnhold/smart/uploads/covers/large_8ed01d0eaa3699f3183715aff2c749405905170a.jpg' },
      { type: 'video/mp4', url: 'http://www.w3schools.com/html/mov_bbb.mp4' }
    ]
  })
  .then(function(res)
  {
    console.log("OK! " + JSON.stringify(res))
  },
  function(err)
  {
    console.error("ERROR! " + err.message + ", at " + err.stack);
  })

// var uuid = require('uuid');

// sb.submitMessageRaw(45, 'facebooksearch',
// {
//   "id": uuid(),
//   "link": null,
//   "from": {
//     "id": uuid(),
//     "name": "name here",
//     "username": "username",
//     "is_verified": false,
//     "link": null,
//     "url": null,
//     "pic": null,
//     "pic_big": null
//   },
//   "message": "this is the message",
//   "type": "status",
//   "post_id": uuid(),
//   "avatar_url": null,
//   "created_time": Math.round(Date.now() / 1000),
//   "updated_time": Math.round(Date.now() / 1000) 
// }).then((ok) => {
//   console.log("ok", ok);
// }, (err) => {
//   console.log("error", err)
// })

// sb.submitMessageRaw(41, 'GenericMessage',
// {
//   "document_type": "GenericMessage",
//   "search_id": 41,
//   "source": "Moderator",
//   "source_type": "Rendered",
//   "author": "author",
//   "nickname": "nickname",
//   "message": "message text",
//   "timestamp": Date.now(),
//   "created": Date.now(),
//   "profile_image_url": null,
//   "images": [],
//   "videos": [],
//   "current_format_id": 0,
//   "format_data": null,
//   "id": null,
//   "origin": null,
//   "links": null,
//   "location": null,
//   "profile_id": null,
//   "permalink": null,
//   "hashtags": null,
//   "usertags": null,
//   "counts": null,
//   "original": null
// }).then((ok) => {
//   console.log("ok", ok);
// }, (err) => {
//   console.log("error", err)
// })

// sb.submitMessageRaw(41, 'documentbin', // 'GenericMessage'
// {
//   "id": uuid(),
//   "author": "Peter Pan",
//   "nickname": "ppan14",
//   "title": "Hello",
//   "message": "this is the message",
//   "timestamp": Date.now()
// }).then((ok) => {
//   console.log("ok", ok);
// }, (err) => {
//   console.log("error", err)
// })