var NeverNoStory = require('../lib/never-no-story');

var story = new NeverNoStory("storyrunetest.never.no");

story.submitVote(
  35, // poll id
  38, // search id
  {
    '91881a9a-35e3-aec1-af27-1542c88287bf': 1, // uuid alt 1
    '1f4f2577-ffa8-4314-87a6-4f8ad64d2068': 2, // uuid alt 2
    '5fe66e16-9537-7358-f455-ed8f62b109d3': 3 // uuid alt 3
  })
  .then(function(res)
  {
    console.log("OK! " + JSON.stringify(res))
  },
  function(err)
  {
    console.error("ERROR! " + err.message + ", at " + err.stack);
  })
